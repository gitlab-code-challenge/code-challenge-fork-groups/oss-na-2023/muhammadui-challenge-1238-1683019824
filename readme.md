# Open Source Summit NA 2023 Code Challenge

## Getting started

Welcome to the Open Source Summit North America 2023 Code Challenge hosted on CodeChallenge.dev. _We're so glad you're here_.

Fork this project into your namespace to get started.

## Level 1

The provided project contains an Image-to-ASCII art converter in Golang and the CI/CD jobs can generate [SLSA-2 attestation](https://about.gitlab.com/blog/2022/11/30/achieve-slsa-level-2-compliance-with-gitlab/) for job artifacts to help achieve SLSA Level 2 compliance with GitLab. To complete this level, you will need to enable the generation of artifacts metadata in the `.gitlab-ci.yml` file. 
Add `RUNNER_GENERATE_ARTIFACTS_METADATA: 1` in the `variables` section and commit. Then proceed to create a Merge Request back to the upstream project.  To view the attestation file generated, navigate into the `build` job details and browse the artifacts to inspect the `artifacts-metadata.json` SLSA attestation file. 
This level is marked as completed once the `RUNNER_GENERATE_ARTIFACTS_METADATA` variable is detected in the MR changes. Note: The pipeline still fails, continue with level 2 to fix it.

## Level 2

For level 2, we will continue from the previous level with the Image-to-ASCII art converter provided in the project, its written in Golang. No worries, there is no deep Golang knowledge required. 
For some reason, the tests fail and block the CI/CD pipeline deployment. Tip: Someone forgot to update the challenge from last year in `main.go` file.    Fix the wrong value in the `helloFrom` variable, commit the changes and inspect the `deploy` job in the CI/CD pipeline.    As a bonus exercise, change the `IMAGE_PATH` variable in `.gitlab-ci.yml` to the proposed value, and see what happens.  
The level will be completed once the corrected value is be detected in `main.go` in the MR changes.

## Level 3

For level 3, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to start. There are many things to contribute to: the Ruby on Rails backend, the Vue-based frontend, the Go-based services like the GitLab Runner and Gitaly, and the documentation for all of those things and more.  
To complete this level, you'll need to contribute and add the `CodeChallenge` label in the Merge Request for visibility, then visit the GitLab booth (P1) for your prize. Prizes are only valid for the duration of the KubeCon EU 2023 event.
You can also participate in one of our upcoming [Hackathons](https://about.gitlab.com/community/hackathon/#sessions).
